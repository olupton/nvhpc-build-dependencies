# NVHPC Build Dependencies
This is a tiny test project to demonstrate that build dependencies are not
correctly handled when the Ninja generator is used with the NVIDIA HPC SDK
compilers.

## Instructions
The shell script `test.sh` configures and builds the project, then updates a
header and rebuilds. With the Makefile generator this rebuilds the executable
but with Ninja it does not.

## Versions
At the time of writing I was using:
 - CMake 3.20.1
 - nvc++ 21.2
 - Ninja 1.10.1
 - GNU Make 3.82

