mkdir build_makefiles build_ninja build_ninja_MD build_ninja_MD_MT
(cd build_makefiles; cmake ..)
(cd build_ninja; cmake -G Ninja ..)
(cd build_ninja_MD; cmake -G Ninja -DCMAKE_DEPFILE_FLAGS_CXX="-MD<DEP_FILE>" ..)
for name in makefiles ninja ninja_MD; do
  (
    cd build_${name}/
    echo "First build with ${name}"
    cmake --build .
    echo "Updating header"
    touch ../header.h
    echo "Rebuilding with ${name}"
    cmake --build .
    echo "Done...main should have been rebuilt."
  )
done
echo "Showing Ninja dependency information"
(cd build_ninja; ninja -t deps)
(cd build_ninja_MD; ninja -t deps)
echo "Trying to build with -MT <DEP_TARGET> in addition"
(cd build_ninja_MD_MT; cmake -G Ninja -DCMAKE_DEPFILE_FLAGS_CXX="-MT <DEP_TARGET> -MD<DEP_FILE>" ..; cmake --build .)
